package com.boots.repository;

import com.boots.entity.RoleEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional(readOnly = true)
public interface RoleRepository extends PagingAndSortingRepository<RoleEntity, Long> {

    @Query("SELECT r FROM role r ORDER BY r.id ASC")
    List<RoleEntity> findAllRole();
}