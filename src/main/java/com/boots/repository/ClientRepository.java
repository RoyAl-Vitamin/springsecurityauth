package com.boots.repository;

import com.boots.entity.ClientEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(readOnly = true)
public interface ClientRepository extends PagingAndSortingRepository<ClientEntity, Long> {

    ClientEntity findByUsername(String username);

    @Query("SELECT c FROM client c WHERE (SELECT r FROM role r WHERE r.name LIKE :name) MEMBER OF c.roles")
    Iterable<ClientEntity> findAllByRoleName(@Param("name") String name);
}