package com.boots.config;

import com.boots.dto.ClientDTO;
import com.boots.dto.RoleDTO;
import com.boots.entity.ClientEntity;
import com.boots.entity.RoleEntity;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ModelMapperConfig {

    @Bean
    public ModelMapper getModelMapper() {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.createTypeMap(ClientEntity.class, ClientDTO.class).addMappings(mapper -> {
            mapper.map(ClientEntity::getId, ClientDTO::setId);
            mapper.map(ClientEntity::getUsername, ClientDTO::setUsername);
            mapper.map(ClientEntity::getPassword, ClientDTO::setPassword);
        });
        modelMapper.createTypeMap(RoleEntity.class, RoleDTO.class).addMappings(mapper -> {
            mapper.map(RoleEntity::getId, RoleDTO::setId);
            mapper.map(RoleEntity::getName, RoleDTO::setName);
        });
        return modelMapper;
    }
}
