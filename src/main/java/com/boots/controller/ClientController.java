package com.boots.controller;

import com.boots.dto.ClientDTO;
import com.boots.dto.RoleDTO;
import com.boots.entity.ClientEntity;
import com.boots.entity.RoleEntity;
import com.boots.repository.ClientRepository;
import com.boots.repository.RoleRepository;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@Validated
public class ClientController {

    private final ClientRepository clientRepository;

    private final RoleRepository roleRepository;

    private final ModelMapper modelMapper;

    public ClientController(ClientRepository clientRepository, RoleRepository roleRepository, ModelMapper modelMapper) {
        this.clientRepository = clientRepository;
        this.roleRepository = roleRepository;
        this.modelMapper = modelMapper;
    }

    @RequestMapping(value = "/getAllClients",
            method = {RequestMethod.POST},
            produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('USER')")
    public Iterable<ClientDTO> getAllClients() {
        List<ClientEntity> clientEntityList = (List<ClientEntity>) clientRepository.findAllByRoleName("USER");
        List<ClientDTO> clientList = new ArrayList<>(clientEntityList.size());
        for (ClientEntity client : clientEntityList) {
            clientList.add(modelMapper.map(client, ClientDTO.class));
        }
        return clientList;
    }

    @RequestMapping(value = "/getAllAdmins",
            method = {RequestMethod.POST},
            produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('ADMIN')")
    @ResponseStatus(HttpStatus.OK)
    public Iterable<ClientDTO> getAllAdmins(/*@RequestBody ClientEntity newUserEntity*/) {
        List<ClientEntity> clientEntityList = (List<ClientEntity>) clientRepository.findAllByRoleName("ADMIN");
        List<ClientDTO> clientList = new ArrayList<>(clientEntityList.size());
        for (ClientEntity client : clientEntityList) {
            clientList.add(modelMapper.map(client, ClientDTO.class));
        }
        return clientList;
    }

    @RequestMapping(value = "/getAllRoles",
            method = {RequestMethod.POST},
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasRole('USER')")
    public Iterable<RoleDTO> getAllRoles() {
        List<RoleEntity> roleEntityList = roleRepository.findAllRole();
        List<RoleDTO> roleList = new ArrayList<>(roleEntityList.size());
        for (RoleEntity role : roleEntityList) {
            roleList.add(modelMapper.map(role, RoleDTO.class));
        }
        return roleList;
    }
}
