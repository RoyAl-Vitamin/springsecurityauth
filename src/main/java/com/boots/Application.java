package com.boots;

import com.boots.entity.ClientEntity;
import com.boots.entity.RoleEntity;
import com.boots.repository.ClientRepository;
import com.boots.repository.RoleRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

@SpringBootApplication
public class Application implements CommandLineRunner {

    private final ClientRepository clientRepository;

    private final RoleRepository roleRepository;

    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    public Application(ClientRepository userRepository, RoleRepository roleRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.clientRepository = userRepository;
        this.roleRepository = roleRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    @Transactional
    public void run(String... args) {
        RoleEntity roleAdmin = new RoleEntity();
        roleAdmin.setName("ADMIN");

        RoleEntity roleUser = new RoleEntity();
        roleUser.setName("USER");

        ClientEntity userAdmin = new ClientEntity();
        userAdmin.setUsername("admin");
        userAdmin.setPassword(bCryptPasswordEncoder.encode("password"));
        userAdmin.setRoles(Set.of(roleAdmin, roleUser));
        roleAdmin.setClients(Set.of(userAdmin));

        ClientEntity userUser = new ClientEntity();
        userUser.setUsername("user");
        userUser.setPassword(bCryptPasswordEncoder.encode("password"));
        userUser.setRoles(Set.of(roleUser));
        roleUser.setClients(Set.of(userAdmin, userUser));

        roleRepository.save(roleAdmin);
        roleRepository.save(roleUser);
        clientRepository.save(userAdmin);
        clientRepository.save(userUser);

        List<ClientEntity> list = (List<ClientEntity>) clientRepository.findAll();
        System.out.println("user list size = [" + list.size() + "]");
        for (ClientEntity user : list) {
            System.out.println("user = [" + user.toString() + "]");
        }
    }
}
