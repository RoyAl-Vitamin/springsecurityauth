## Spring Security Example

App show how to authentication using [HTTP header](https://developer.mozilla.org/en-US/docs/Web/HTTP/Authentication#Security_of_basic_authentication) `Authorization` like a `Authorization: <user>:<password>`. Authentication user and his role store in the DB. While the app is upping start Spring method create user and role and places in the DB. Every REST-method annotated `@PreAuthorize("hasRole('ROLE_*')")`. This guarantees the differentiation of access. Every request must be authorized.

### How to run DB

Create new docker container

```terminal
docker pull postgres
docker run --name postgres-for-prj -e POSTGRES_USER=sysdba -e POSTGRES_PASSWORD=masterkey -e POSTGRES_DB=authdb -p 5432:5432 -d postgres
```

or start an existing container

```terminal
docker start postgres-for-prj
```

If you want to see data in the DB then use psql. [To learn more.](https://dev.to/shree_j/how-to-install-and-run-psql-using-docker-41j2)

### How to run java app

Build app

```terminal
gradlew build
```

and start app

```terminal
java -jar springsecurityauth-1.0-SNAPSHOT.jar
```

### How to check if it works

In `script` folder you see `.sh` files. Run files, and you see result in `output.json`

### Links

About Spring Security: [1](https://habr.com/ru/post/203318/), [2](https://habr.com/ru/post/482552/), [3](https://habr.com/ru/company/otus/blog/488418/)